import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-what-event',
  templateUrl: './what-event.component.html',
  styleUrls: ['./what-event.component.css']
})
export class WhatEventComponent implements OnInit {
	attendence;
	totalDay;
	eventName;
	event = {};
	drawObj = [];
  constructor() { }
  showChart : boolean = true;
  showButton : boolean = false;
  showModal : boolean = false;
  ngOnInit() {
  	this.clearCanvas();
  }

  onSubmit(){
  	console.log(this.event);
  	var startDate,endDate;
  	startDate = this.event["startDate"].split("-");
  	startDate = startDate[1] + '/' + startDate[2] + '/' + startDate[0];
  	endDate = this.event["endDate"].split("-");
  	endDate = endDate[1] + '/' + endDate[2] + '/' + endDate[0];

  	var d = new Date();
  	var todayDate = String((d.getMonth() + 1) + '/' + d.getDate() + '/' + d.getFullYear());
  	console.log(todayDate, startDate,endDate);
  	var totalDays = this.daysDiff(startDate,endDate);
  	var totalWeeks = totalDays/7;
  	var totalMonths = totalDays/30;
  	var totalYears = totalDays/365;
  	var totalDaysAttended;
  	totalDaysAttended = this.daysDiff(startDate,todayDate);
  	if (this.event["freqType"]==1) {
  		// totalDaysAttended = this.event["eventAtten"] * totalWeeks;
  		totalDays = totalWeeks * this.event["eventFreq"];
  		totalDaysAttended = (totalDaysAttended/7) * this.event["eventFreq"];
  	}else{
  		if (this.event["freqType"]==2) { 
  			// totalDaysAttended = this.event["eventAtten"] * totalMonths;
  			totalDays = totalMonths * this.event["eventFreq"];
  			totalDaysAttended = (totalDaysAttended/30) * this.event["eventFreq"];
  		} else {
  			// totalDaysAttended = this.event["eventAtten"] * totalYears;
  			totalDays = totalYears * this.event["eventFreq"];
  			totalDaysAttended = (totalDaysAttended/365) * this.event["eventFreq"];
  		}
  	}
  	console.log(startDate,endDate,totalDays,totalDaysAttended);
  	this.attendence = Math.floor(totalDaysAttended);
  	this.totalDay = Math.floor(totalDays);
  	this.draw(totalDays,totalDaysAttended,3);
  	this.drawObj[0] = totalDays;
  	this.drawObj[1] = totalDaysAttended;
  	this.eventName = this.event["name"];
  	this.event = {};
  	this.showButton = !this.showButton;
  	this.showModal = !this.showModal;
  }

  draw(totalCircles: number, coloredCircles: number,mode: number){
    var radius,gap;
    if(mode == 1){
      if (totalCircles < 7) {
      	totalCircles = 0;
      }
    	totalCircles = totalCircles/7;
    	coloredCircles = coloredCircles/7;
      radius = 8;
      gap = 25;
    }
    if (mode == 2) { 
    	if(totalCircles < 30){
      	totalCircles = 0;
      }
    	totalCircles = totalCircles/30;
    	coloredCircles = coloredCircles/30;
      radius = 11;
      gap = 30;
      
  	}
  	if (mode == 3) {
  		totalCircles = totalCircles;
    	coloredCircles = coloredCircles;
      radius = 5;
      gap = 16;
  	}
    var canvas = <HTMLCanvasElement> document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    var rows = Math.floor(Math.sqrt(totalCircles));
    var x = rows;
    var canvasWidth = 12.5 + x*gap + 12.5;
    var canvasHeight = 12.5 + ( x + ((totalCircles) - x*x)/x + 1)*gap + 12.5;
    ctx.canvas.width = canvasWidth;
    ctx.canvas.height = canvasHeight;
    console.log(canvasWidth,canvasHeight);
    var noOfCircles = 0
    for (var i = 0; noOfCircles <  totalCircles; i++) {
      if (noOfCircles == totalCircles) {
        break;
      }
      for (var j = 0; j < rows; j++) {
        if (noOfCircles == totalCircles) {
          break;
        }
        ctx.strokeStyle = 'rgb(32,190,208)';
        ctx.beginPath();
        ctx.arc(12.5 + j * gap, 12.5 + i * gap, radius, 0, Math.PI * 2, true);
        ctx.stroke();
        if (noOfCircles < coloredCircles) {
          ctx.fillStyle = 'rgb(116, 164, 242)';
        }else{
          ctx.fillStyle = 'rgb(255, 255, 255)';
        }
        ctx.fill();
        noOfCircles = noOfCircles + 1;
      }
    }
  }

  drawCanvas(mode:number){
  	this.clearCanvas();
  	this.draw(this.drawObj[0],this.drawObj[1],mode);
  }

  daysDiff(date1: any,date2: any){
  	var d1 = new Date(date1);
  	var d2 = new Date(date2);
  	return Math.floor((Date.UTC(d2.getFullYear(),d2.getMonth(),d2.getDate()) - Date.UTC(d1.getFullYear(),d1.getMonth(),d1.getDate()))/(1000*60*60*24));
  }

  clearCanvas(){
    var canvas = <HTMLCanvasElement> document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.canvas.width = 0;
    ctx.canvas.height = 0;
  }

  showForm(){
  	this.clearCanvas();
    console.log("canvas cleared");
    this.showModal = !this.showModal;
    this.showButton = !this.showButton;
  }

}
