import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatEventComponent } from './what-event.component';

describe('WhatEventComponent', () => {
  let component: WhatEventComponent;
  let fixture: ComponentFixture<WhatEventComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatEventComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatEventComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
