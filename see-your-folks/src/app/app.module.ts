import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { Http, Response, HttpModule } from '@angular/http';
import {FormsModule} from "@angular/forms";
import { QueryFormComponent } from './query-form/query-form.component';
import { WhatEventComponent } from './what-event/what-event.component';

@NgModule({
  declarations: [
    AppComponent,
    QueryFormComponent,
    WhatEventComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot([
      { 
        path: 'seeyourfolks',
        component : QueryFormComponent
      },
      {
        path: '',
        component : WhatEventComponent
      },
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
