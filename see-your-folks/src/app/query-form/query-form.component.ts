import { Component, OnInit } from '@angular/core';
import { Http, Response, HttpModule } from '@angular/http';
// import 'rxjs/add/operator/map';
//import { Observable } from 'rxjs/Observable';
import { QueryService } from '../data.services';
@Component({
  selector: 'app-query-form',
  templateUrl: './query-form.component.html',
  styleUrls: ['./query-form.component.css'],
  providers: [QueryService]
})
export class QueryFormComponent implements OnInit {

  baseJsonPath = 'https://jsonfiles-8bed8.firebaseapp.com/';
  jsonData = {};
  query = {};
  result: number;
  showModal: boolean = false;
  //flag: boolean = false;
  jsonObj = [];
  countries = ["Bangladesh","Brazil","China","India","Indonesia","Mexico","Nigeria","Pakistan","Russia","UnitedStates"];
  showDayChart : boolean = true;
  showButtons : boolean = false;
  drawObj = [];
  prevMode = 0;

  constructor(private http: Http) {
    // console.log(this.flag);
    var jsonPath,temp;
    for(let country of this.countries){
      jsonPath = this.baseJsonPath + country.toLowerCase() + ".json";
      this.http.get(jsonPath).subscribe(
        data => temp = data.json(),
        error => console.log(error),
        () => this.jsonObj.push(temp)
      ); 
    }
  }

  ngOnInit() {
    this.clearCanvas();
  }

  calAgeGroup(age : number) : string{
    var start = 0;
    while(age >= start){
      start = start + 5;
    }
    start = start - 5;
    if(start >= 85){
      return "85+ years"
    }else{
      return start.toString() + "-" + (start + 4).toString() + " years";
    }
  }

  onSubmit(){
    var obj = this.jsonObj[this.countries.indexOf(this.query["country"])];
    console.log(this.countries.indexOf(this.query["country"]));
    console.log(obj);
    obj = obj["fact"];
    var gho = "ex - expectation of life at age x";
    var ageGroup;
    var motherExpectedAge,fatherExpectedAge;
    console.log(obj[0]["dims"],obj.length);
    ageGroup = this.calAgeGroup(Number(this.query["mAge"]));
    console.log(ageGroup);
    for (var i = 0; i < obj.length; i++) {
        if (obj[i]["dims"]["GHO"] == gho) {
          if (obj[i]["dims"]["AGEGROUP"] == ageGroup) {
            if (obj[i]["dims"]["SEX"] == "Female") {
              if (obj[i]["dims"]["YEAR"] == "2014") {
                motherExpectedAge = parseInt(obj[i]["Value"]);
                break;
              }
            }
          }
        }
    }
    ageGroup = this.calAgeGroup(Number(this.query["fAge"]));
    console.log(ageGroup);
    for (var i = 0; i < obj.length; i++) {
        if (obj[i]["dims"]["GHO"] == gho) {
          if (obj[i]["dims"]["AGEGROUP"] == ageGroup) {
            if (obj[i]["dims"]["SEX"] == "Male") {
              if (obj[i]["dims"]["YEAR"] == "2014") {
                fatherExpectedAge = parseInt(obj[i]["Value"]);
                break;
              }
            }
          }
        }
    }

    
    console.log(motherExpectedAge,fatherExpectedAge);
    var parentsExpectedAge;
    if(motherExpectedAge < fatherExpectedAge){
      this.result = motherExpectedAge * this.query["daysInYear"];
      parentsExpectedAge = motherExpectedAge;
    }else{
      this.result = fatherExpectedAge * this.query["daysInYear"];
      parentsExpectedAge = fatherExpectedAge;
    }
    var yourAge = this.query["yourAge"];
    
    var yourExpectedAge;
    ageGroup = this.calAgeGroup(Number(yourAge));
    console.log(ageGroup);
    for (var i = 0; i < obj.length; i++) {
        if (obj[i]["dims"]["GHO"] == gho) {
          if (obj[i]["dims"]["AGEGROUP"] == ageGroup) {
            if (obj[i]["dims"]["SEX"] == "Male") {
              if (obj[i]["dims"]["YEAR"] == "2014") {
                yourExpectedAge = parseInt(obj[i]["Value"]);
                break;
              }
            }
          }
        }
    }
    console.log(yourExpectedAge);
    //this.showModal = true;
    this.query = {};
    //this.showDayChart = !this.showDayChart;
    this.showModal = !this.showModal;
    //this.showDayChart = !this.showDayChart;
    this.draw(yourAge, parentsExpectedAge ,yourExpectedAge,1);
    this.drawObj[0] = yourAge;
    this.drawObj[1] = parentsExpectedAge;
    this.drawObj[2] = yourExpectedAge;
    this.prevMode = 1;
    this.showButtons = !this.showButtons;
  }

  draw(yourAge: number, parentsExpectedAge:number, yourExpectedAge: number,mode : number){
    var radius,gap;
    if(mode == 48){
      radius = 5;
      gap = 16;
    }else{
      radius = 8;
      gap = 25;
    }

    var canvas = <HTMLCanvasElement> document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    var totalYears = yourExpectedAge + yourAge;
    totalYears = totalYears * mode;
    var rows = Math.floor(Math.sqrt(totalYears));
    var x = rows;
    var canvasWidth = 12.5 + x*gap + 12.5;
    var canvasHeight = 12.5 + ( x + ((totalYears) - x*x)/x + 1)*gap + 12.5;
    ctx.canvas.width = canvasWidth;
    ctx.canvas.height = canvasHeight;
    console.log(canvasWidth,canvasHeight);
    var noOfCircles = 0
    for (var i = 0; noOfCircles <  totalYears; i++) {
      if (noOfCircles == totalYears) {
        break;
      }
      for (var j = 0; j < rows; j++) {
        if (noOfCircles == totalYears) {
          break;
        }
        ctx.strokeStyle = 'rgb(32,190,208)';
        ctx.beginPath();
        ctx.arc(12.5 + j * gap, 12.5 + i * gap, radius, 0, Math.PI * 2, true);
        ctx.stroke();
        if (noOfCircles < yourAge * mode) {
          ctx.fillStyle = 'rgb(116, 164, 242)';
        }else{
          if(noOfCircles < (yourAge + parentsExpectedAge) * mode) {
            ctx.fillStyle = 'rgb(255, 255, 255)';
          }else{
            ctx.fillStyle = 'rgb(217, 233, 247)';
          }
        }
        ctx.fill();
        noOfCircles = noOfCircles + 1;
      }
    }
  }

  clearCanvas(){
    var canvas = <HTMLCanvasElement> document.getElementById("canvas");
    var ctx = canvas.getContext("2d");
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    ctx.canvas.width = 0;
    ctx.canvas.height = 0;
  }

  showForm(){
    //this.location.reload();
    this.clearCanvas();
    console.log("canvas cleared");
    this.showModal = !this.showModal;
    this.showButtons = !this.showButtons;
  }

  drawCanvas(mode : number){
    if(mode != this.prevMode){
      this.clearCanvas();
      this.draw(this.drawObj[0],this.drawObj[1],this.drawObj[2],mode);
      this.prevMode = mode;
    }
  }
}
